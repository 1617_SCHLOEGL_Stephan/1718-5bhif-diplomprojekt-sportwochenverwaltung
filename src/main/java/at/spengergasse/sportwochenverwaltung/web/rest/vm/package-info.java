/**
 * View Models used by Spring MVC REST controllers.
 */
package at.spengergasse.sportwochenverwaltung.web.rest.vm;
